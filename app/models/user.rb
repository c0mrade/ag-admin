class User
  include Mongoid::Document
  ### Attributes ###
  field :app_user_id, type: Integer
  field :sso_id, type: Integer
  field :first_name, type: String
  field :last_name, type: String
  field :email, type: String
  field :active, type: Boolean
  field :entitlements, type: Array, default: ->{ [] }

  ### Indexes ###
  index({ sso_id: 1 }, unique: true)
  index(app_user_id: 1)
  ### Constants ###

  ### Associations ###

  ### Validations ###
  validates_presence_of :sso_id
  validates_uniqueness_of :sso_id
  validates_presence_of :app_user_id

  ### Scopes ###

  ### Callbacks ###

  ### Delegation ###

  ### Plugins ###

  ### Class methods ###
  def self.from_token(token)
    decoded = Auth.decode(token)
    User.where(decoded.slice('sso_id')).first
  end

  ### Instance methods ###
  # User might be having different entitlements at
  # the time of the user creation vs when delete comment
  # occurs. And equality only really matters using sso_id
  # cause if the user changes the sso, it still retains the same app_user_id
  # so technically there could be 2 users with the same app_user_id
  # but different sso_id. That should make them 2 different users
  def ==(other)
    other.class == self.class && other.sso_id == sso_id
  end

  def profile
    return nil unless active?

    attributes
    .symbolize_keys
    .slice(:first_name, :last_name, :sso_id, :entitlements)
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
