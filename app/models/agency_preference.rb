# This Class represents parent agency preferece
class AgencyPreference
  include Mongoid::Document
  include Mongoid::Timestamps

  ### Attributes ###
  field :parent_agency_id, type: Integer
  field :final_spend, type: Boolean

  ### Indexes ###
  index({ parent_agency_id: 1 }, { unique: true })

  ### Constants ###

  ### Associations ###

  ### Validations ###
  validates :parent_agency_id, presence: true
  validates :final_spend, presence: true

  ### Scopes ###

  ### Callbacks ###

  ### Delegation ###

  ### Plugins ###

  ### Class methods ###

  ### Instance methods ###
end
