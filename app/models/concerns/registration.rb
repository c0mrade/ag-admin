# Concern responsible for adding fields and related helper methods to models
# that deal with registrations.
module Registration
  extend ActiveSupport::Concern

  included do
    REGISTRATION_FIELDS = {
      pre_cy_registration: 'Registrations 3Q17',
      q1_cy_registration: 'Registrations 4Q17',
      q2_cy_registration: 'Registrations 1Q18',
      q3_cy_registration: 'Registrations 2Q18',
      q4_cy_registration: 'Registrations 3Q18',
      post_cy_registration: 'Registrations 4Q18'
    }.freeze

    REGISTRATION_FIELDS.each_key  do |fld|
      field fld, type: Integer
    end

    def total_registration
      REGISTRATION_FIELDS.keys.map { |field|
        send(field) || 0
      }.sum
    end
  end
end
