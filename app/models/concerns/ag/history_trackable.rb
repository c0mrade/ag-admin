module AG::HistoryTrackable
  extend ActiveSupport::Concern

  included do
    attr_accessor :user, :audits
    before_save :init_deal_history
    after_save :persist_deal_history, unless: -> { audits.nil? || audits.empty? }

    class << self
      attr_reader :trackable_fields, :field_names

      # Fields you want to track options[:on]
      def track_history(options = {})
        @trackable_fields = options[:on]
      end
    end

    private

    # List of key/values pairs that we care about
    # containing old values
    def old_attributes
      changed_attributes.symbolize_keys.slice(*self.class.trackable_fields.keys)
    end

    def init_deal_history
      return if old_attributes.empty?
      @audits = []

      old_attributes.each do |name, value|
        deal_history = DealHistory.new(old_value: value,
                                       new_value: send(name),
                                       human_readable_name: human_readable_name(name),
                                       field_name: name)
        deal_history.user = user
        audits << deal_history
      end
    end

    def persist_deal_history
      audits.each do |deal_history|
        deal_history.deal_id = deal_id
        deal_history.save!
      end
    end

    def human_readable_name(field_name)
      self.class.trackable_fields[field_name]
    end
  end
end
