class SmsDeal
  include Mongoid::Document
  include AG::HistoryTrackable
  include Registration

  ### Attributes ###
  field :deal_id, type: Integer
  field :_id, default: -> { deal_id }
  field :marketplace_id, type: Integer
  field :marketplace_name, type: String
  field :deal_name, type: String
  field :multi_year, type: Boolean, default: false
  field :parent_agency_name, type: String
  field :parent_agency_id, type: Integer
  field :property_name, type: String
  field :agency_id, type: Integer
  field :agency_name, type: String
  field :advertiser_name, type: String
  field :demographic_name, type: String
  field :deal_tag_name, type: String
  field :deal_tag_id, type: Integer
  field :vertical_name, type: String
  field :vertical_id, type: Integer
  field :spend_dollars, type: Integer
  field :prior_budget_id, type: Integer
  field :guaranteed_impressions, type: Integer
  field :authorized_user_ids, type: Array, default: -> { [] }
  field :selling_vertical_id, type: Integer
  field :selling_vertical_name, type: String
  field :current_budget_id, type: Integer
  field :cy_ask_dollars, type: Integer
  field :box_url, type: String

  ### Indexes ###
  index({ authorized_user_ids: 1 })
  index({ deal_id: 1 }, { unique: true })

  ### Constants ###
  EXCLUDE_FROM_PAYLOAD = %i(authorized_user_ids) + REGISTRATION_FIELDS.keys
  SMS_MARKETPLACES = {
    upfront: 10000,
    calendar: 10002,
    scatter: 10003,
    olympics: 10004,
    super_bowl: 10005,
    world_cup: 10006
  }.freeze
  ALLOWED_MARKETPLACE_IDS = SMS_MARKETPLACES.slice(
    :upfront,
    :olympics,
    :super_bowl,
    :world_cup
  ).values.freeze

  ### Associations ###
  has_one  :agency_deal, foreign_key: :deal_id, dependent: :destroy
  has_many :deal_histories, foreign_key: :deal_id, order: 'created_at desc', dependent: :destroy

  ### Validations ###
  validates_presence_of :deal_id
  validates_uniqueness_of :deal_id

  ### Scopes ###
  scope :all_upfront_deals, (lambda do |user|
    where(authorized_user_ids: user.app_user_id)
      .where(:marketplace_id.in => ALLOWED_MARKETPLACE_IDS)
      .without(EXCLUDE_FROM_PAYLOAD)
      .includes(:agency_deal)
      .order_by(agency_name: 'asc', property_name: 'asc', advertiser_name: 'asc')
  end)

  scope :py_upfront_deals, (lambda do |user|
    all_upfront_deals(user)
      .where('spend_dollars' => { '$gt' => 0 })
  end)

  ### Callbacks ###

  ### Delegation ###

  ### Plugins ###
  track_history on: {
    marketplace_name: 'Marketplace',
    agency_name: 'Agency'
  }

  ### Class methods ###

  # Maybe as an additional protection
  # associate SSO to agency in PAM
  def self.parent_agency_ids(user)
    where(authorized_user_ids: user.app_user_id)
      .where(:marketplace_id.in => ALLOWED_MARKETPLACE_IDS)
      .distinct(:parent_agency_id)
  end

  ### Instance methods ###

  def cpm
    if guaranteed_impressions? && guaranteed_impressions != 0
      return ((nbcu_spend.to_f / guaranteed_impressions.to_f) * 1_000).round(2)
    end
    0
  end

  # If there is an associated agency_spend object, and the value for its
  # spend_dollar attribute is not nil, then return the spend_dollars for that
  # agency_deal. In all other cases -- i.e. there is no agency_deal, or the
  # agency_deal's spend_dollars are nil, return nbcu_spend.
  def agency_spend
    agency_deal? ? (agency_deal.spend_dollars || nbcu_spend) : nbcu_spend
  end

  def nbcu_spend
    spend_dollars || 0
  end

  def variance
    agency_spend - (nbcu_spend || 0)
  end

  def as_json(options = {})
    super(
      {
        except: [:_id],
        include: {
          agency_deal: {
            include: {
              comments: {
                include: {
                  user: {
                    only: %i(first_name last_name app_user_id)
                  }
                }
              }
            },
            except: %i(deal_id _id created_at updated_at)
          }
        }
      }.merge(options)
    )
  end
end
