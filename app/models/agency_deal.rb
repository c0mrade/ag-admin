class AgencyDeal
  include Mongoid::Document
  include Mongoid::Timestamps
  include AG::HistoryTrackable
  include Registration

  ### Attributes ###
  field :deal_id, type: Integer
  field :spend_dollars, type: Integer
  ### Indexes ###
  index({ deal_id: 1 }, unique: true)

  ### Constants ###
  REGISTRATION_ATTRIBUTES = %i(deal_id) + REGISTRATION_FIELDS.keys

  ### Associations ###
  belongs_to :sms_deal, foreign_key: :deal_id
  embeds_many :comments

  ### Validations ###
  validates :sms_deal, presence: true
  validates_uniqueness_of :deal_id
  validates_numericality_of :spend_dollars, unless: ->(ad) { ad.spend_dollars.blank? }

  ### Scopes ###

  ### Callbacks ###

  ### Delegation ###

  ### Plugins ###
  track_history on: REGISTRATION_FIELDS

  ### Class methods ###
  def self.bulk_create_or_update(model_attributes, user)
    model_attributes.each do |ma|
      # Need to have at least 2 keys to save
      # deal_id is one, and any attribute
      next if ma.keys.size == 1

      deal_id = ma.delete(:deal_id)
      ad = AgencyDeal.find_or_initialize_by(deal_id: deal_id)
      ad.attributes = ma
      ad.user = user
      ad.save!
    end
  end

  # This action creates comments for
  # a given agency deal.
  # Since comments are sent to NBCU along
  # with spend_dollars they're part of
  # this model as they're horizontally aligned.
  # This method will first delete all previous
  # comments, then just save the ones
  def self.create_comments!(options)
    deal_id = options[:deal_id].to_i
    # This means either nil or invalid value was provided
    raise AG::InvalidSmsDeal if deal_id == 0
    user = options[:user]
    comments = options[:comments]

    ad = AgencyDeal.find_or_initialize_by(deal_id: deal_id)
    ad.comments.delete_all unless ad.new_record?

    comments.each do |ca|
      ad.comments.build(ca.merge(user: user))
      ad.save!
    end
  end

  # We should probably use aggregate framework
  # and $lookup to find data from sms deals
  # and aggregate it into Mongo::Collection::View::Aggregation
  # it would be more efficient that way than iterating trough all them deals
  def self.send_to_sms(parent_agency_id)
    data = {
      parent_agency_id: parent_agency_id,
      agency_deals: [],
      message_type: 'agency_deals'
    }

    # doesn't work when I want to override as_json
    # on the comment model
    # TODO investigate how to do this on the model
    # and incorporate in the sms_deal as_json
    comments_json_format = {
      only: [:text, :created_at],
      include: {
        user: {
          only: [:app_user_id]
        }
      }
    }

    includes(:sms_deal).to_a.each do |ag_deal|
      sms_deal = ag_deal.sms_deal
      next if sms_deal.parent_agency_id != parent_agency_id

      data[:agency_deals] << {
        deal_id: sms_deal.deal_id,
        current_budget_id: sms_deal.current_budget_id,
        prior_budget_id: sms_deal.prior_budget_id,
        spend_dollars: ag_deal.spend_dollars,
        comments: ag_deal.comments.to_json(comments_json_format),
        pre_cy_registration: ag_deal.pre_cy_registration,
        q1_cy_registration: ag_deal.q1_cy_registration,
        q2_cy_registration: ag_deal.q2_cy_registration,
        q3_cy_registration: ag_deal.q3_cy_registration,
        q4_cy_registration: ag_deal.q4_cy_registration,
        post_cy_registration: ag_deal.post_cy_registration
      }
    end

    data
  end
end
