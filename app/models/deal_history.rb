class DealHistory
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  ### Attributes ###
  field :old_value, type: String
  field :new_value, type: String
  field :human_readable_name, type: String
  field :field_name, type: String
  field :deal_id, type: Integer

  ### Indexes ###
  index(deal_id: 1)

  ### Constants ###

  ### Associations ###
  belongs_to :user, optional: true
  belongs_to :sms_deal, optional: true, foreign_key: :deal_id

  ### Validations ###
  validates :deal_id, presence: true

  ### Scopes ###
  scope :for_deal_id, (lambda do |*deal_ids|
    where(:deal_id.in => deal_ids)
      .order_by(created_at: 'desc')
      .limit(20)
  end)

  scope :without_create_audit, (lambda do
    where(:field_name.in => %i(marketplace_name agency_name) + Registration::REGISTRATION_FIELDS.keys)
    .not_in(old_value: nil)
  end)

  ### Callbacks ###

  ### Delegation ###

  ### Plugins ###

  ### Class methods ###

  ### Instance methods ###
  def created_at_formatted
    created_at.in_time_zone('Eastern Time (US & Canada)').strftime('%b %-d, %Y %I:%M %p %Z')
  end

  def modified_by
    user ? user.full_name : ''
  end
end
