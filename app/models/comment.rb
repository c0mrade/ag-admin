# This Class represents AgencyDeal comment
class Comment
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  ### Attributes ###
  field :text, type: String

  ### Indexes ###

  ### Constants ###

  ### Associations ###
  embedded_in :agency_deal
  embeds_one  :user

  ### Validations ###
  validates :text, presence: true
  validates :agency_deal, presence: true
  validates :user, presence: true

  ### Scopes ###

  ### Callbacks ###

  ### Delegation ###

  ### Plugins ###

  ### Class methods ###

  ### Instance methods ###
end
