ActiveAdmin.register SmsDeal do
  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :deal_id
    column :marketplace_name
    column :deal_name
    column :parent_agency_name
    column :property_name
    column :agency_name
    column :advertiser_name
    column :demographic_name
    column :deal_tag_name
    column :vertical_name
    column :selling_vertical_name
    actions
  end

  filter :deal_id, label: "DEAL ID"
  filter :marketplace_name
  filter :deal_name
  filter :parent_agency_name
  filter :property_name
  filter :agency_name
  filter :advertiser_name
  filter :demographic_name
  filter :deal_tag_name
  filter :vertical_name
  filter :selling_vertical_name

  form do |f|
    f.inputs "User Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :sso_id
    end
    f.actions
  end

end
