ActiveAdmin.register User do
  permit_params :first_name, :last_name, :email, :sso_id

  index do
    selectable_column
    column :app_user_id
    column :first_name
    column :last_name
    column :sso_id
    column :active
    column :email
    column :entitlements
    actions
  end

  filter :first_name
  filter :last_name
  filter :app_user_id
  filter :sso_id
  filter :app_user_id
  filter :entitlements

  form do |f|
    f.inputs "User Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :sso_id
    end
    f.actions
  end

end
